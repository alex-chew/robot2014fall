package com.spcrobotics;

import edu.wpi.first.wpilibj.Relay;

public class Constant
{
	// Global slots
	public static final int SLOT_LEFT			= 1;
	public static final int SLOT_RIGHT			= 2;
	public static final int SLOT[] =
	{
		SLOT_LEFT,	// FL
		SLOT_RIGHT,	// FR
		SLOT_LEFT,	// RL
		SLOT_RIGHT	// RR
	};
	
	// Global encoders
	public static final int ENC_COUNTS_PER_REV	= 500; // DO NOT MODIFY! Experimentally determined
	
	//.Operator interface
	// @TODO fill in
	public static final int PORT_DRIVER			= 2;
	public static final int PORT_OPERATOR		= 1;
	
	public static final int BTN_ARM_DOWN        = 4;
	public static final int BTN_ARM_UP          = 2;
	public static final int BTN_ROLLER_IN		= 5;
	public static final int BTN_ROLLER_OUT		= 6;
	public static final int BTN_SHOOT_RESET		= 1;
	public static final int BTN_SHOOT_FIRE		= 3;
	public static final double DEADZONE_JOYMAG	= 0.18;
	public static final double DEADZONE_JOYROT	= 0.1;
	public static final double DEADZONE_PS2MOV	= 0.15;
	public static final double DEADZONE_PS2ROT	= 0.15;
	
	// Test mode
	public static final int JOYSTICK_MAG_LINE	= 1;
	public static final int JOYSTICK_DIR_LINE	= 2;
	public static final int JOYSTICK_ROT_LINE	= 3;
	public static final int BUTTONS_LINE		= 4;
	
	// Drivetrain Victors
	public static final int DRIVE_PWM[] =
	{
		4,	// FL
		4,	// FR
		2,	// RL
		1	// RR
	};
	
	// Direction multipliers: +1 for normal direction Victor, -1 for reverse
	public static final int[] DRIVE_VIC_MULTIPLIERS =
	{
		1,	// FL
	   -1,	// FR
		1,	// RL
	   -1	// RR
	};
	
	// Drivetrain encoder channels
	public static final int DRIVE_ENC[][] =
	{
		{7,		8},		// FL
		{7,		8},		// FR
		{13,	14},	// RL
		{13,	14},	// RR
	};
	
	// Drivetrain encoder distance per pulse
	// @TODO fill in
	public static final double[] DRIVE_ENC_DISTPERPULSE =
	{
		0.0,	// FL
		0.0,	// FR
		0.0,	// RL
		0.0		// RR
	};
	
	// Drivetrain Victor PID constants
	// @TODO fill in
	public static final double[][] DRIVE_PID =
	{
		{0.02,		0.0,		0.001},	// FL
		{0.02,		0.0,		0.001},	// FR
		{0.05,		0.0,		0.001},	// RL
		{0.02,		0.0,		0.001}	// RR
	};
	
	// Drivetrain miscellaneous
	public static final double DRIVE_AUTO_DIST	= 8.0 * 12.0;	// 8 ft * 12 in/ft
	public static final int __FL__				= 0;
	public static final int __FR__				= 1;
	public static final int __RL__				= 2;
	public static final int __RR__				= 3;
	
	// Drivetrain gyro
	public static final int DRIVE_GYRO_SLOT				= 1;
	public static final int DRIVE_GYRO_CHAN				= 6;
	public static final double DRIVE_GYRO_K_P_OFFSET	= 1 / 20;	// 0.05 V/deg
	
	// NOTE: This only allows us to correct for less than +/- 100deg. If we're
	// more off than this, something's gone horribly wrong.
	public static final double DRIVE_GYRO_K_P_MULTIPLIER	= 0.01;	// 0.01 V/deg
	
	public static final String DRIVE_NAME[] =
	{
		"frontLeft",
		"frontRight",
		"rearLeft",
		"rearRight"
	};
	
	// Roller
	// @TODO fill in
	public static final int ROLLER_SLOT = 1;
	public static final int ROLLER_CHANNEL = 10;
	public static final Relay.Value ROLLER_DIR_IN	= Relay.Value.kReverse;
	public static final Relay.Value ROLLER_DIR_OUT	= Relay.Value.kForward;
	public static final Relay.Value ROLLER_DIR_OFF	= Relay.Value.kOff;
	
	// Arm compressor and solenoid 
	public static final int ARM_SLOT = 1;
	public static final int ARM_COMP_PRESSURE_CHAN = 1;
	public static final int ARM_COMP_RELAY_CHAN = 8;
	public static final int ARM_SOL_IN_CHAN = 1;
	public static final int ARM_SOL_OUT_CHAN = 2;
	
	// Arm solenoid states
	public static final boolean ARM_DOWN_IN = false;
	public static final boolean ARM_DOWN_OUT = true;
	public static final boolean ARM_UP_IN = true;
	public static final boolean ARM_UP_OUT = false;
	
//	// Arm Victors/encoders
//	public static final int ARM_VIC_L			= 5;
//	public static final int ARM_VIC_R			= 5;
//	public static final int ARM_ENC_L_A			= 9;
//	public static final int ARM_ENC_L_B			= 10;
//	public static final int ARM_ENC_R_A			= 9;
//	public static final int ARM_ENC_R_B			= 10;
//	
//	// Arm PID constants (NOT USED)
//	public static final double ARM_L_K_P		= 0.0;
//	public static final double ARM_L_K_I		= 0.0;
//	public static final double ARM_L_K_D		= 0.0;
//	public static final double ARM_R_K_P		= 0.0;
//	public static final double ARM_R_K_I		= 0.0;
//	public static final double ARM_R_K_D		= 0.0;
//	
//	// Arm setpoints and limit switches
//	// @TODO fill in
//	public static final double ARM_SETPT_UP		= 0.0;
//	public static final double ARM_SETPT_DOWN	= 0.0;
//	public static final int ARM_LIMSWITCH_SLOT	= 1;
//	public static final int ARM_LIMSWITCH_FRONT	= 11;
//	public static final int ARM_LIMSWITCH_REAR	= 10;
	
	// Catapult Victors/encoders
	// @TODO fill in
	public static final int CAT_VIC_L			= 1;		// Left Victor
	public static final int CAT_VIC_R			= 2;		// Right Victor
	
	// Catapult PID constants
	// @TODO fill in
	public static final double CAT_L_K_P		= 0.0;		// Left kP
	public static final double CAT_L_K_I		= 0.0;		// Left kI
	public static final double CAT_L_K_D		= 0.0;		// Left kD
	public static final double CAT_R_K_P		= 0.0;		// Right kP
	public static final double CAT_R_K_I		= 0.0;		// Right kI
	public static final double CAT_R_K_D		= 0.0;		// Right kD
	
	// Catapult misc.
	// @TODO fill in
	public static final double CAT_SPEED_RESET	= 0.0;		// Speed for resetting
	public static final double CAT_SPEED_FIRE	= 0.0;		// Speed for firing
	public static final double CAT_SETPT_RESET	= 0.0;		// Setpoint for resetting
	public static final double CAT_SETPT_FIRE	= 0.0;		// Setpoint for firing
	
	// Absolute encoder pins
	public static final int ABSENC_SLOT			= 2;
	public static final int ABSENC_PIN_CLK		= 5;		// Clock
	public static final int ABSENC_PIN_MOSI		= 6;		// Data out
	public static final int ABSENC_PIN_MISO		= 4;		// Data in
	public static final int ABSENC_PIN_L_CS		= 2;		// Left Chip selects
	public static final int ABSENC_PIN_R_CS		= 3;		// Right Chip selects
	// NOTE: Right pins were assumed same as left pins

	// Absolute encoder messages
	public static final int ABSENC_MSG_NO_OP	= 0x00;		// No operation
	public static final int ABSENC_MSG_RDPOS	= 0x10;		// GET position
	public static final int ABSENC_MSG_SETZERO	= 0x70;		// SET position 0
	public static final int ABSENC_MSG_WAIT		= 0xA5;		// [from Enc] Wait for data
	public static final int ABSENC_MSG_DONE		= 0x80;		// End of data
	
	// Analog channels (logging)
	public static final int ANALOG_MODULE = 1;
	public static final int[] ANALOG_CHAN = {1, 2, 3, 4, 5, 7};
	public static final String[] ANALOG_NAME =
	{
		"an1",
		"an2",
		"an3",
		"an4",
		"an5",
		"an7"
	};
}
