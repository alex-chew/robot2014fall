package com.spcrobotics.peripheral;

import com.spcrobotics.Constant;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.SPIDevice;

// Was named AMT203Encoder
public class AbsoluteEncoder
	implements PIDSource
{
	SPIDevice _spi = null;
	long result;
	long position;
	public boolean valid;

	public AbsoluteEncoder
	(
		DigitalOutput clk,
		DigitalOutput mosi,
		DigitalInput miso,
		DigitalOutput cs
	)
	{
		// Init SPI
		_spi = new SPIDevice(clk, mosi, miso, cs);
		
		// Request reset, and wait for completion
		result = _spi.transfer(Constant.ABSENC_MSG_SETZERO, 8);
		while (result == Constant.ABSENC_MSG_WAIT)
		{
			result = _spi.transfer(Constant.ABSENC_MSG_NO_OP, 8);
		}
	
	}

	public double pidGet() {return getEncoderPosition();}

	public long getEncoderPosition()
	{
		// Local vars
		long localResult;
		long localPosition = 0L;
		
		// Pos not validated yet
		valid = false;
		
		// Request read position, wait for completion
		localResult = _spi.transfer(Constant.ABSENC_MSG_RDPOS, 8);
		while (localResult == Constant.ABSENC_MSG_WAIT)
		{
			localResult = _spi.transfer(Constant.ABSENC_MSG_NO_OP, 8);
		}

		// Um... process output I guess? Fancy bitwise operations
		if (localResult == Constant.ABSENC_MSG_RDPOS)
		{
			localResult		= _spi.transfer(Constant.ABSENC_MSG_NO_OP, 8);
			localPosition	= (localResult & 0xF);
			localPosition	= localPosition << 8;
			localResult		= _spi.transfer(Constant.ABSENC_MSG_NO_OP, 8);
			localPosition	+= (localResult & 0xFF);
			valid = true;
		}
		else {System.out.println("Encoder read failed.");}
		
		return localPosition;
	}
	
	public void setZero()
	{
		System.out.println("Resetting SPI device.");
		result = _spi.transfer(Constant.ABSENC_MSG_SETZERO, 8);
		while (result == Constant.ABSENC_MSG_WAIT)
		{
			result = _spi.transfer(Constant.ABSENC_MSG_NO_OP, 8);
		}
	}
}
