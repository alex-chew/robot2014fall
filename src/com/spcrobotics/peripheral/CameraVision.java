package com.spcrobotics.peripheral;

import com.spcrobotics.common.FlightRecorder;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.camera.AxisCamera;
import edu.wpi.first.wpilibj.image.*;

public class CameraVision
{
	private static CameraVision _singleton = null;
	
	private CameraVision()
	{
		camera = AxisCamera.getInstance();  // get an instance of the camera
		cc = new CriteriaCollection();      // create the criteria for the particle filter
		cc.addCriteria(NIVision.MeasurementType.IMAQ_MT_AREA, AREA_MINIMUM, 65535, false);
		records = FlightRecorder.getInstance();
		matchTime.start();
	}
	
	public static CameraVision getInstance()
	{
		if (_singleton == null) {_singleton = new CameraVision();}
		return _singleton;
	}
	
	final int Y_IMAGE_RES = 480;
	final double VIEW_ANGLE = 49;
	
	final double PI = 3.141592653;
	
	final int RECTANGULARITY_LIMIT = 40;
	final int ASPECT_RATIO_LIMIT = 55;
	
	final int TAPE_WIDTH_LIMIT = 50;
	final int VERTICAL_SCORE_LIMIT = 50;
	final int LR_SCORE_LIMIT = 50;
	
	final int AREA_MINIMUM = 50;
	
	final int MAX_PARTICLES = 8;
	boolean timeToSaveHotGoalImage = true;
	boolean timeToSaveNotGoalImage = true;
	boolean wasHot = false;
	boolean isHot = true;
	static CameraVision singletonCamera;
	AxisCamera camera;          // the axis camera object (connected to the switch)
	CriteriaCollection cc;      // the criteria for doing the particle filter operation
	Solenoid cameraSpike = new Solenoid(1);
	Timer matchTime = new Timer();
	FlightRecorder records;

	public class Scores
	{
		double rectangularity;
		double aspectRatioVertical;
		double aspectRatioHorizontal;
	}

	public class TargetReport
	{
		int verticalIndex;
		int horizontalIndex;
		boolean Hot;
		double totalScore;
		double leftScore;
		double rightScore;
		double tapeWidthScore;
		double verticalScore;
	};

	public void checkCamera()
	{
		CameraVision.TargetReport target = new CameraVision.TargetReport();
		int verticalTargets[] = new int[MAX_PARTICLES];
		int horizontalTargets[] = new int[MAX_PARTICLES];
		int verticalTargetCount, horizontalTargetCount;
		cameraSpike.set(true);
		try
		{
			ColorImage image = camera.getImage();
			System.out.println("Got image");
			BinaryImage thresholdImage = image.thresholdHSV(105, 137, 230, 255, 133, 183);  // ******************************** // keep only green objects

			BinaryImage filteredImage = thresholdImage.particleFilter(cc);
			CameraVision.Scores scores[] = new CameraVision.Scores[filteredImage.getNumberParticles()];
			horizontalTargetCount = verticalTargetCount = 0;

			if (filteredImage.getNumberParticles() > 0)
			{

				for (int i = 0; i < MAX_PARTICLES && i < filteredImage.getNumberParticles(); i++)
				{
					ParticleAnalysisReport report = filteredImage.getParticleAnalysisReport(i);
					scores[i] = new CameraVision.Scores();
					System.out.println("filteredImage.getNumberParticles() > 0, that's graced");
					scores[i].rectangularity = scoreRectangularity(report);
					scores[i].aspectRatioVertical = scoreAspectRatio(filteredImage, report, i, true);
					scores[i].aspectRatioHorizontal = scoreAspectRatio(filteredImage, report, i, false);

					if (scoreCompare(scores[i], false))
					{
						System.out.println("particle: " + i + "is a Horizontal Target centerX: " + report.center_mass_x + "centerY: " + report.center_mass_y);
						horizontalTargets[horizontalTargetCount++] = i;
					}
					else if (scoreCompare(scores[i], true))
					{
						System.out.println("particle: " + i + "is a Vertical Target centerX: " + report.center_mass_x + "centerY: " + report.center_mass_y);
						verticalTargets[verticalTargetCount++] = i;
					}
					else
					{
						System.out.println("particle: " + i + "is not a Target centerX: " + report.center_mass_x + "centerY: " + report.center_mass_y);
					}
					System.out.println("rect: " + scores[i].rectangularity + "ARHoriz: " + scores[i].aspectRatioHorizontal);
					System.out.println("ARVert: " + scores[i].aspectRatioVertical);
				}
				
				target.totalScore = target.leftScore = target.rightScore = target.tapeWidthScore = target.verticalScore = 0;
				target.verticalIndex = verticalTargets[0];
				for (int i = 0; i < verticalTargetCount; i++)
				{
					ParticleAnalysisReport verticalReport = filteredImage.getParticleAnalysisReport(verticalTargets[i]);
					for (int j = 0; j < horizontalTargetCount; j++)
					{
						ParticleAnalysisReport horizontalReport = filteredImage.getParticleAnalysisReport(horizontalTargets[j]);
						double horizWidth, horizHeight, vertWidth, leftScore, rightScore, tapeWidthScore, verticalScore, total;

						horizWidth = NIVision.MeasureParticle(filteredImage.image, horizontalTargets[j], false, NIVision.MeasurementType.IMAQ_MT_EQUIVALENT_RECT_LONG_SIDE);
						vertWidth = NIVision.MeasureParticle(filteredImage.image, verticalTargets[i], false, NIVision.MeasurementType.IMAQ_MT_EQUIVALENT_RECT_SHORT_SIDE);
						horizHeight = NIVision.MeasureParticle(filteredImage.image, horizontalTargets[j], false, NIVision.MeasurementType.IMAQ_MT_EQUIVALENT_RECT_SHORT_SIDE);

						leftScore = ratioToScore(1.2 * (verticalReport.boundingRectLeft - horizontalReport.center_mass_x) / horizWidth);
						rightScore = ratioToScore(1.2 * (horizontalReport.center_mass_x - verticalReport.boundingRectLeft - verticalReport.boundingRectWidth) / horizWidth);
						tapeWidthScore = ratioToScore(vertWidth / horizHeight);
						verticalScore = ratioToScore(1 - (verticalReport.boundingRectTop - horizontalReport.center_mass_y) / (4 * horizHeight));
						total = leftScore > rightScore ? leftScore : rightScore;
						total += tapeWidthScore + verticalScore;

						if (total > target.totalScore)
						{
							target.horizontalIndex = horizontalTargets[j];
							target.verticalIndex = verticalTargets[i];
							target.totalScore = total;
							target.leftScore = leftScore;
							target.rightScore = rightScore;
							target.tapeWidthScore = tapeWidthScore;
							target.verticalScore = verticalScore;
						}
					}
					target.Hot = hotOrNot(target);
				}

				if (verticalTargetCount > 0)
				{
					if (target.Hot)
					{
						System.out.println("Hot target located");
						System.out.println(matchTime.get());
					}
					else {System.out.println("No hot target present");}
				}

			}
			else if (timeToSaveNotGoalImage) {timeToSaveNotGoalImage = false;}

			if (timeToSaveHotGoalImage) {timeToSaveHotGoalImage = false;}
			filteredImage.free();
			thresholdImage.free();
			image.free();

			if (timeToRecord(target) == true)
			{
				records.write("Hot target was:" + isHot + "at " + matchTime.get());
			}
		}
		catch (Exception ex) {ex.printStackTrace();}

	}

	public double scoreAspectRatio(BinaryImage image, ParticleAnalysisReport report, int particleNumber, boolean vertical)
		throws NIVisionException
	{
		double rectLong, rectShort, aspectRatio, idealAspectRatio;

		rectLong = NIVision.MeasureParticle(image.image, particleNumber, false, NIVision.MeasurementType.IMAQ_MT_EQUIVALENT_RECT_LONG_SIDE);
		rectShort = NIVision.MeasureParticle(image.image, particleNumber, false, NIVision.MeasurementType.IMAQ_MT_EQUIVALENT_RECT_SHORT_SIDE);
		idealAspectRatio = vertical ? (4.0 / 32) : (23.5 / 4);	// Vertical reflector 4" wide x 32" tall, horizontal 23.5" wide x 4" tall

		if (report.boundingRectWidth > report.boundingRectHeight)
		{
			aspectRatio = ratioToScore((rectLong / rectShort) / idealAspectRatio);
		}
		else
		{
			aspectRatio = ratioToScore((rectShort / rectLong) / idealAspectRatio);
		}
		return aspectRatio;
	}

	boolean scoreCompare(CameraVision.Scores scores, boolean vertical)
	{
		boolean isTarget = true;

		isTarget &= scores.rectangularity > RECTANGULARITY_LIMIT;
		if (vertical)
		{
			isTarget &= scores.aspectRatioVertical > ASPECT_RATIO_LIMIT;
		}
		else
		{
			isTarget &= scores.aspectRatioHorizontal > ASPECT_RATIO_LIMIT;
		}

		return isTarget;
	}
	
	double scoreRectangularity(ParticleAnalysisReport report)
	{
		if (report.boundingRectWidth * report.boundingRectHeight != 0)
		{
			return 100 * report.particleArea / (report.boundingRectWidth * report.boundingRectHeight);
		}
		else {return 0;}
	}

	double ratioToScore(double ratio)
	{
		return (Math.max(0, Math.min(100 * (1 - Math.abs(1 - ratio)), 100)));
	}

	public boolean timeToRecord(CameraVision.TargetReport target)
	{
		boolean bothHot = (wasHot == isHot);
		wasHot = isHot;
		return !bothHot;
	}

	boolean hotOrNot(CameraVision.TargetReport target)
	{
		boolean hot = true;

		hot &= target.tapeWidthScore >= TAPE_WIDTH_LIMIT;
		hot &= target.verticalScore >= VERTICAL_SCORE_LIMIT;
		hot &= (target.leftScore > LR_SCORE_LIMIT) | (target.rightScore > LR_SCORE_LIMIT);
		return hot;
	}
}
