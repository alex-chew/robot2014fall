package com.spcrobotics.common.display;

/**
 * @author Alex
 */
public class ScrollDisplay extends Display
{
	private static ScrollDisplay _singleton = null;
	
	String[] _data;
	
	// Just to prevent extra instances
	private ScrollDisplay()
	{
		_data = new String[super.MAX_LENGTH];
	}
	
	// Short for getInstance()
	public static ScrollDisplay get()
	{
		if (_singleton == null)
		{
			_singleton = new ScrollDisplay();
		}
		
		return _singleton;
	}
	
	public void print(String msg)
	{
		// Shift data up
		shiftDataBack();
		
		// Put message to last element
		_data[_data.length - 1] = msg;
		
		// Print data array
		super.print(_data);
	}
	
	private void shiftDataBack()
	{
		// Loop through the array's indices, except the last index
		for (int i = 0; i < (_data.length - 1); i++)
		{
			// Element 1 is shifted into 0, E2 into 1, etc.
			_data[i] = _data[i + 1];
		}
	}
}
