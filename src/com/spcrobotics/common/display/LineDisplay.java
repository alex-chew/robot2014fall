package com.spcrobotics.common.display;

/**
 * @author Alex
 */
public class LineDisplay extends Display
{
	private static LineDisplay _singleton = null;
	
	String[] _data;
	
	// Just to prevent extra instances
	private LineDisplay()
	{
		_data = new String[super.MAX_LENGTH];
	}
	
	// Short for getInstance()
	public static LineDisplay get()
	{
		if (_singleton == null)
		{
			_singleton = new LineDisplay();
		}
		
		return _singleton;
	}
	
	public void print(String msg, int line)
	{
		// If line is invalid, return without printing anything
		if (line < 0 && line >= _data.length) {return;}
		
		// Set line to msg
		_data[line] = msg;
		
		// Print data array
		super.print(_data);
	}
}
