package com.spcrobotics.common.display;

import edu.wpi.first.wpilibj.DriverStationLCD;

/**
 * @author Alex
 */
public abstract class Display
{
	public static final int LINES = DriverStationLCD.kNumLines;
	public static final int MAX_LENGTH = DriverStationLCD.kLineLength;
	
	private void print(String msg, DriverStationLCD.Line ln)
	{
		// If message is too long, truncate
		String trimmed =
				(msg.length() > MAX_LENGTH) ?
				msg.substring(0, MAX_LENGTH) : msg;
		
		// Print
		DriverStationLCD.getInstance().println(ln, 1, trimmed);
	}
	
	protected void print(String[] data)
	{
		DriverStationLCD.getInstance().clear();
		print(data[0], DriverStationLCD.Line.kUser1);
		print(data[1], DriverStationLCD.Line.kUser2);
		print(data[2], DriverStationLCD.Line.kUser3);
		print(data[3], DriverStationLCD.Line.kUser4);
		print(data[4], DriverStationLCD.Line.kUser5);
		print(data[5], DriverStationLCD.Line.kUser6);
	}
}
