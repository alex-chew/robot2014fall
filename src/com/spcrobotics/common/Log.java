package com.spcrobotics.common;

public class Log
{
	public static void start()
	{
		FlightRecorder.getInstance();
		AnalogLogger.getInstance();
	}
	
	/**
	 * Prints a header (init) message to the FlightLog for a full PID subsystem.
	 * Must be run once for each subsystem that should be logged.
	 * 
	 * @param name			name of the subsystem
	 * @param vicChannel	channel of the Victor
	 * @param quadA			channel A of the Encoder
	 * @param quadB			channel B of the Encoder
	 * @param p				kP of the PID controller
	 * @param i				kI of the PID controller
	 * @param d				kD of the PID controller
	 */
	public static void frHeader(String name, int vicChannel, int quadA, int quadB, double p, double i, double d, String units)
		// @TODO add units!
	{
		FlightRecorder.getInstance().writeHeader(name, vicChannel, quadA, quadB, p, i, d);
	}
	
	/**
	 * Prints a header (init) message to the FlightLog for a non-PID subsystem
	 * (doesn't actually use full PID).
	 * 
	 * @param name	name of the subsystem
	 */
	public static void frHeader(String name)
	{
		FlightRecorder.getInstance().writeHeader(name, "0", "null", "null", 0.0, 0.0, 0.0);
	}
	
	/**
	 * Prints an entry message to the FlightLog.
	 * 
	 * @param name		name of the subsystem
	 * @param actual	actual/current value of the input
	 * @param desired	desired value (or setpoint) of the output controller
	 * @param control	actual value being sent to the output
	 */
	public static void frEntry(String name, double actual, double desired, double control)
	{
		FlightRecorder.getInstance().writeEntry(null, actual, desired, control);
	}
	
	/**
	 * Prints a status (miscellaneous) message to the FlightLog. It will be in
	 * the format "name: msg".
	 * 
	 * @param name	name of the subsystem
	 * @param msg	message to be printed
	 */
	public static void frStatus(String name, String msg)
	{
		FlightRecorder.getInstance().write(name + ": " + msg);
	}
	
	/**
	 * Prints a message to both the FlightLog and standard output stream (usually
	 * the console/cmdline).
	 * 
	 * @param msg	message to be printed
	 */
	public static void frAndPrint(String msg)
	{
		System.out.println(msg);
		FlightRecorder.getInstance().write(msg);
	}
	
	/**
	 * Logs analog channels and battery, should be run once per period.
	 */
	public static void analog()
	{
		AnalogLogger.getInstance().log();
	}
}
