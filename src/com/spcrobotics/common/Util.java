package com.spcrobotics.common;

public class Util
{
	/**
	 * Returns a "curved" value: that is, it is normalized and scaled, and
	 * softened by squaring it (while retaining the sign).
	 * 
	 * @param raw	the raw value to be curved
	 * @param deadzone	the deadzone positive bound
	 * @return	the curved value
	 */
	public static double curveInput(double raw, double deadzone)
	{
		// Normalize-scale
		double normalizeScaled = MathUtil.normalizeScale(raw, deadzone);
		
		// Soften
		return MathUtil.sign(raw) * MathUtil.pow(normalizeScaled, 2);
	}
	
	/**
	 * Returns an array of wheel speeds for each of four wheels (FL, FR, RL, RR)
	 * in that order, from the magnitude, direction, and rotation of an input axis
	 * (a joystick).
	 * 
	 * @param mag	magnitude of input axis
	 * @param dir	direction of input axis
	 * @param rot	rotation (twist) of input axis
	 * @return	an array of wheel speeds for each of four wheels (FL, FR, RL, RR)
	 */
	public static double[] calculateWheelsFromDirection(double mag, double dir, double rot)
	{
		double FL = 0.0D;
		double FR = 0.0D;
		double RL = 0.0D;
		double RR = 0.0D;
		
		// Sector 1
		if (dir >= 0 && dir < 45)
		{
			FL = mag;
			FR = mag * ((dir / 45) - 1);
			RL = mag * ((dir / 45) - 1);
			RR = mag;
		}
		
		// Sector 2
		else if (dir >= 45 && dir < 90)
		{
			FL = mag;
			FR = mag * ((dir - 45) / 45);
			RL = mag * ((dir - 45) / 45);
			RR = mag;
		}
		
		// Sector 3
		else if (dir >= 90 && dir < 135)
		{
			FL = -mag * (((dir - 90) / 45) - 1);
			FR = mag;
			RL = mag;
			RR = -mag * (((dir - 90) / 45) - 1);
		}
		
		// Sector 4
		else if (dir >= 135 && dir < 180)
		{
			FL = -mag * ((dir - 135) / 45);
			FR = mag;
			RL = mag;
			RR = -mag * ((dir - 135) / 45);
		}
		
		// Sector 5
		else if (dir >= 180 && dir < 225)
		{
			FL = -mag;
			FR = -mag * (((dir - 180) / 45) - 1);
			RL = -mag * (((dir - 180) / 45) - 1);
			RR = -mag;
		}
		
		// Sector 6
		else if (dir >= 225 && dir < 270)
		{
			FL = -mag;
			FR = -mag * ((dir - 225) / 45);
			RL = -mag * ((dir - 225) / 45);
			RR = -mag;
		}
		
		// Sector 7
		else if (dir >= 270 && dir < 315)
		{
			FL = mag * (((dir - 270) / 45) - 1);
			FR = -mag;
			RL = -mag;
			RR = mag * (((dir - 270) / 45) - 1);
		}
		
		// Sector 8
		else if (dir >= 315 && dir < 360)
		{
			FL = mag * ((dir - 315) / 45);
			FR = -mag;
			RL = -mag;
			RR = mag * ((dir - 315) / 45);
		}
		
		// Rotation
		FL += rot;
		FR -= rot;
		RL += rot;
		RR -= rot;
		
		// Value arrays
		double[] rawWheels = {FL, FR, RL, RR};
		double[] wheels = new double[rawWheels.length];
		
		// If abs() > 1, set to 1 while retaining direction
		for (int i = 0; i < rawWheels.length; i++)
		{
			if (rawWheels[i] > 1)			{wheels[i] = 1;}
			else if (rawWheels[i] < -1)		{wheels[i] = -1;}
			else							{wheels[i] = rawWheels[i];}
		}
		
		return wheels;
	}
}
