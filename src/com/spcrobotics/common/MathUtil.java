package com.spcrobotics.common;

public class MathUtil
{
	public static double pow(double x, int y)
	{
		if (x == 0 || x == 1 || y == 1) {return x;}
		if (y < 0) {return 0;}
		if (y == 0) {return 1;}
		
		double res = x;
		for (int i = 0; i < y - 1; i++)
		{
			res *= x;
		}
		
		return res;
	}
	
	public static int sign(double x)
	{
		return (x > 0) ? 1 : -1;
	}
	
	/**
	 * Returns a normalized value from the RAW parameter; that is, 0.0 if within
	 * the deadzone, or the value itself otherwise.
	 * 
	 * @param raw	the raw value to be normalized
	 * @param deadzone	the deadzone positive bound
	 * 
	 * @return	the normalized value
	 */
	public static double normalize(double raw, double deadzone)
	{
		if (raw < deadzone && raw > (-1 * deadzone))
		{
			return 0.0D;
		}
		
		return raw;
	}
	
	/**
	 * Returns a normalized, scaled value from the RAW parameter.
	 * <p>
	 * Let x be the absolute value of the RAW parameter. If x is between 0 and
	 * the deadzone, 0.0 is returned. Otherwise, it is scaled between 0 and 1.
	 * Finally, the sign of the RAW parameter is restored. (This method is very
	 * difficult to explain. Write a program to iterate over some inputs and
	 * print the outputs. Alternatively, graph it and all will become clear.)
	 * 
	 * @param raw	the raw value to be normalize-scaled
	 * @param deadzone	the deadzone positive bound
	 * @return	the normalized, scaled value
	 */
	public static double normalizeScale(double raw, double deadzone)
	{
		// [-1, -D) or (D, 1]
		if
		(
			((-1 <= raw) && (raw < -1 * deadzone)) ||
			((deadzone < raw) && (raw <= 1))
		)
		{
			return (raw - sign(raw) * deadzone) / (1 - deadzone);
		}
		else if (raw < -1)
		{
			return -1;
		}
		else if (raw > 1)
		{
			return 1;
		}
		else
		{
			// This case occurs if we are in the deadzone
			return 0.0D;
		}
	}
	
	/**
	 * Converts an absolute direction (-180deg to 180deg) to the Cartesian system
	 * (0deg to 360deg). This can be used to convert joystick input into math-
	 * compatible values for calculation.
	 * 
	 * @param absoluteDirection	the absolute direction in degrees
	 * @return the direction, in degrees, converted to the Cartesian system
	 */
	public static double directionToCartesian(double absoluteDirection)
	{
		if ((absoluteDirection <= 90) && (absoluteDirection >= -180))
		{
			return (90 - absoluteDirection);
		}
		else if ((absoluteDirection <= 180) && (absoluteDirection > 90))
		{
			return (450 - absoluteDirection);
		}
		else
		{
			return absoluteDirection;
		}
	}
	
	public static int round(double d)
	{
		return (int) Math.floor(d + 0.5);
	}
}
