package com.spcrobotics.common;

import com.sun.squawk.microedition.io.FileConnection;
import edu.wpi.first.wpilibj.Timer;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import javax.microedition.io.Connector;

public class FlightRecorder
{

	private static FlightRecorder _singleton;
	
	private int maxNum = 3; // Maximum number of files you want to keep.
	private String _filename = "file:///fileIndex.txt";
	
	private FileConnection f;
	private FileConnection indexFile;
	private OutputStream _outStream;
	private OutputStreamWriter _outWriter;
	private InputStream _inStream;
	private InputStreamReader _inReader;
	
	private char cbuf[] = new char[30];
	
	private int fileID = 0;
	private int totalFileNum = 0;
	
	Timer time = null;

	private FlightRecorder()
	{
		try
		{
			time = new Timer();
			time.reset();
			time.start();

			indexFile = (FileConnection) Connector.open(_filename);
			if (!(indexFile.exists()))
			{
				fileID = 0;
				totalFileNum = 0;
			}
			else
			{
				f = (FileConnection) Connector.open(_filename, Connector.READ);
				_inStream = f.openInputStream();
				
				_inReader = new InputStreamReader(_inStream);
				_inReader.read(cbuf);
				f.close();
				
				int index = 0;
				while ((cbuf[index] >= 48) && (cbuf[index] <= 57) && (index < cbuf.length))
				{
					fileID *= 10;
					fileID += (cbuf[index] - 48);
					index++;
				}
				
				index++;
				while ((cbuf[index] >= 48) && (cbuf[index] <= 57) && (index < cbuf.length))
				{
					totalFileNum *= 10;
					totalFileNum += (cbuf[index] - 48);
					index++;
				}
			}

			fileID++;
			totalFileNum++;

			if (fileID > maxNum) {fileID = 1;}

			f = (FileConnection) Connector.open(_filename, Connector.WRITE);
			f.create();
			_outStream = f.openOutputStream();
			_outWriter = new OutputStreamWriter(_outStream);
			_outWriter.write(fileID + "," + totalFileNum);
			close();

			f = (FileConnection) Connector.open("file:///LogFile." + fileID + ".txt", Connector.WRITE);
			f.create();
			_outStream = f.openOutputStream();
			_outWriter = new OutputStreamWriter(_outStream);

			write("Run ID # is:" + Integer.toString(totalFileNum));
			close();

		}
		catch (Exception ex) {ex.printStackTrace();}
	}

	public static FlightRecorder getInstance()
	{
		if (_singleton == null) {_singleton = new FlightRecorder();}
		return _singleton;
	}

	public void write(String value)
	{
		try
		{
			if (_outWriter != null)
			{
				_outWriter.write(time.get() + ": " + value + "\r\n");
			}
		}
		catch (Exception ex) {ex.printStackTrace();}

	}

	public void close()
	{
		try
		{
			_outWriter.flush();
			_outStream.flush();
			f.close();
		}
		catch (Exception ex) {ex.printStackTrace();}
	}
	
	public void writeHeader
	(
		String name, String vicChannel,
		String quadA, String quadB,
		double p, double i, double d)
	{
		write
		(
			"Initialzed PID subsystem \"" + name + "\", " +
			"Victor channel = " + vicChannel + ", " +
			name + "QuadA=" + quadA + ", " +
			name + "QuadB=" + quadB
		);
		write
		(
			name + " parameters are " +
			"P = " + p + ", " +
			"I = " + i + ", " +
			"D = " + d
		);
	}
	
//	public void writeHeader
//	(
//	)
//	{
//		
//	}
	
	public void writeHeader
	(
		String name, int vicChannel,
		int quadA, int quadB,
		double p, double i, double d)
	{
		writeHeader
		(
			name, String.valueOf(vicChannel),
			String.valueOf(quadA), String.valueOf(quadB),
			p, i, d
		);
	}
	
	public void writeEntry
	(
		String name,
		double actual,
		double desired,
		double control
	)
	{
		write
		(
			name + ":" +
			actual + "," +
			desired + "," +
			control
		);
	}
}
