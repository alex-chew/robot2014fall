package com.spcrobotics.common;

import com.spcrobotics.Constant;
import edu.wpi.first.wpilibj.AnalogChannel;
import edu.wpi.first.wpilibj.DriverStation;

public class AnalogLogger
{
	private static AnalogLogger _singleton = null;
	
	private AnalogChannel[] _analogs = new AnalogChannel[Constant.ANALOG_CHAN.length];
	
	private AnalogLogger()
	{
		for (int i = 0; i < _analogs.length; i++)
		{
			_analogs[i] = new AnalogChannel(Constant.ANALOG_MODULE, Constant.ANALOG_CHAN[i]);
			
			// Init msgs
			FlightRecorder.getInstance().write
			(
				"Initialzed PID subsystem \"" + Constant.ANALOG_NAME[i] +
				"\", Victor channel = " + _analogs[i].getChannel() + ", "+
				Constant.ANALOG_NAME[i] + "QuadA=" + "null" + ", " +
				Constant.ANALOG_NAME[i] + "QuadB=" + "null"
			);
			FlightRecorder.getInstance().write
			(
				Constant.ANALOG_NAME[i] + " parameters are " +
				"P = " + 0 +
				", I = " + 0 +
				", D = " + 0
			);
		}
		
		// Init msgs for battery
		FlightRecorder.getInstance().write
		(
			"Initialzed PID subsystem \"" + "batt" +
			"\", Victor channel = " + 8 + ", "+
			"batt" + "QuadA=" + "null" + ", " +
			"batt" + "QuadB=" + "null"
		);
		FlightRecorder.getInstance().write
		(
			"batt" + " parameters are " +
			"P = " + 0 +
			", I = " + 0 +
			", D = " + 0
		);
	}
	
	public static AnalogLogger getInstance()
	{
		if (_singleton == null) {_singleton = new AnalogLogger();}
		return _singleton;
	}
	
	public void log()
	{
		for (int i = 0; i < _analogs.length; i++)
		{	
			String log = "";
			log += Constant.ANALOG_NAME[i] + ":";
			log += _analogs[i].getVoltage() + ",";
			log += "0,0";
			FlightRecorder.getInstance().write(log);
		}
		
		String log = "";
		log += "batt" + ":";
		log += DriverStation.getInstance().getBatteryVoltage() + ",";
		log += "0,0";
		FlightRecorder.getInstance().write(log);
	}
}
