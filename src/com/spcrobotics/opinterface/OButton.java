package com.spcrobotics.opinterface;

import edu.wpi.first.wpilibj.Joystick;

public class OButton
{
	private Joystick joy;
	private int btn;
	private boolean prevState;
	private boolean nextState;
	
	public static final int WAS_PRESSED = 1;
	public static final int WAS_RELEASED = 2;
	public static final int IS_ACTIVATED = 3;
	
	public OButton(int port, int button)
	{
		joy = new Joystick(port);
		btn = button;
		prevState = false;
		nextState = false;
	}
	
	public void update()
	{
		prevState = nextState;
		nextState = joy.getRawButton(btn);
	}
	
	public boolean wasPressed()
	{
		return (!prevState && nextState);
	}
	
	public boolean wasReleased()
	{
		return (prevState && !nextState);
	}
	
	public boolean isActivated()
	{
		return nextState;
	}
	
	public boolean getState(int type)
	{
		switch (type)
		{
			case WAS_PRESSED:
				return wasPressed();
			case WAS_RELEASED:
				return wasReleased();
			case IS_ACTIVATED:
				return isActivated();
			default:
				return false;
		}
	}
}
