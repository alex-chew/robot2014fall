package com.spcrobotics.opinterface;

import com.spcrobotics.Constant;
import edu.wpi.first.wpilibj.Joystick;

public class OI
{
	private static OI _singleton = null;
	
	private static Joystick _driverJoystick;
	private static Joystick _operJoystick;
	private static OButton _armDownButton;
	private static OButton _armUpButton;
	private static OButton _rollerInButton;
	private static OButton _rollerOutButton;
	private static OButton _shootResetButton;
	private static OButton _shootFireButton;
	
	private OI()
	{
		_driverJoystick =		new Joystick(Constant.PORT_DRIVER);
		_operJoystick =			new Joystick(Constant.PORT_OPERATOR);
		_armDownButton =			new OButton(Constant.PORT_OPERATOR, Constant.BTN_ARM_DOWN);
		_armUpButton =			new OButton(Constant.PORT_OPERATOR, Constant.BTN_ARM_UP);
		_rollerInButton =		new OButton(Constant.PORT_OPERATOR, Constant.BTN_ROLLER_IN);
		_rollerOutButton =		new OButton(Constant.PORT_OPERATOR, Constant.BTN_ROLLER_OUT);
		_shootResetButton =		new OButton(Constant.PORT_OPERATOR, Constant.BTN_SHOOT_RESET);
		_shootFireButton =		new OButton(Constant.PORT_OPERATOR, Constant.BTN_SHOOT_FIRE);
	}
	
	public static OI get()
	{
		if (_singleton == null)
		{
			_singleton = new OI();
		}
		
		return _singleton;
	}
	
	public void update()
	{
		_armDownButton.update();
		_armUpButton.update();
		_rollerInButton.update();
		_rollerOutButton.update();
		_shootResetButton.update();
		_shootFireButton.update();
	}
	
	public Joystick getJoyDrive()			{return _driverJoystick;}
	public Joystick getJoyOper()			{return _operJoystick;}
	public boolean getArmDown(int type)		{return _armDownButton.getState(type);}
	public boolean getArmUp(int type)		{return _armUpButton.getState(type);}
	public boolean getRollerIn(int type)	{return _rollerInButton.getState(type);}
	public boolean getRollerOut(int type)	{return _rollerOutButton.getState(type);}
	public boolean getShootReset(int type)	{return _shootResetButton.getState(type);}
	public boolean getShootFire(int type)	{return _shootFireButton.getState(type);}
}
