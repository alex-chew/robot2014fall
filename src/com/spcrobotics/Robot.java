package com.spcrobotics;

import com.spcrobotics.common.AnalogLogger;
import com.spcrobotics.drive.*;
import com.spcrobotics.opinterface.*;
import com.spcrobotics.pickup.*;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Watchdog;

public class Robot
	extends edu.wpi.first.wpilibj.IterativeRobot {
	
	private AutoDrive           _auto = null;
	private TankDrive           _drive = null;
	private ArmPistonController _arm = null;
	private SpeedRoller         _roller = null;
	
	private int count = 0;
	
	public void robotInit() {
		_auto = AutoDrive.getInstance();
		_drive = TankDrive.getInstance();
		_arm = ArmPistonController.getInstance();
		_roller = SpeedRoller.getInstance();
		
		AnalogLogger.getInstance();	
		
		System.out.println("Initialization done.");
	}

	public void autonomousInit() {
		// Set victors to 0; stop/reset encoders
		_auto.resetIO();
		
		// Start encoders; turn on controllers
		for (int i = 0; i < 4; i++) {
			_auto.getBaseSystem(i).ensureSystemEnabled();
		}
	}
	
	/*
	 * Arm down while roller in		40	140
	 * Drive forward				140	260
	 * Fire							260	-->
	 */
	public void autonomousPeriodic() {	
		count++;
		if (count < 40) {
			// Nothing here
		} else if (count >= 40 && count < 140) {
//			_roller.setIn();
		} else if (count >= 140 && count < 260) {
//			_roller.setOff();
			_auto.driveStraight(Constant.DRIVE_AUTO_DIST / 4);
		} if (count >= 260) {
			_auto.stop();
		}
		
		AnalogLogger.getInstance().log();
		Watchdog.getInstance().feed();
	}
	
	public void teleopInit() {}

	public void teleopPeriodic() {
		OI.get().update();
		
		runDrive();
		runArm();
		runRoller();
		
		AnalogLogger.getInstance().log();
		Watchdog.getInstance().feed();
	}
	
	public void testInit() {}
	
	public void testPeriodic() {
		AnalogLogger.getInstance().log();
		Watchdog.getInstance().feed();
	}
	
	public void disabledInit() {
		// So we can run auto multiple times
		count = 0;
	}
	
	private void runDrive() {
		// Tank drive
		_drive.execTele(
			OI.get().getJoyDrive().getAxis(Joystick.AxisType.kY),
			OI.get().getJoyDrive().getAxis(Joystick.AxisType.kZ)
		);
	}
	
	private void runArm() {
		boolean armDown = OI.get().getJoyOper().getRawButton(Constant.BTN_ARM_DOWN);
		boolean armUp = OI.get().getJoyOper().getRawButton(Constant.BTN_ARM_UP);
		
		if (armDown) {
			_arm.armDown();
		} else if (armUp) {
			_arm.armUp();
		}
	}
	
	private void runRoller() {
		if (OI.get().getJoyOper().getRawButton(Constant.BTN_ROLLER_IN)) {
			_roller.setIn();
		} else if (OI.get().getJoyOper().getRawButton(Constant.BTN_ROLLER_OUT)) {
			_roller.setOut();
		} else {
			_roller.setOff();
		}
	}
}
