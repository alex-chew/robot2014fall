package com.spcrobotics.catapult;

import com.spcrobotics.opinterface.OI;

public class CamSync
{
	static final int btnLFwd = 1;	// X
	static final int btnLRev = 2;	// Y
	static final int btnRFwd = 3;	// A
	static final int btnRRev = 4;	// B
	static final int btnZero = 5;	// LB
	
	public static void run()
	{
		boolean lFwd = OI.get().getJoyDrive().getRawButton(btnLFwd);
		boolean lRev = OI.get().getJoyDrive().getRawButton(btnLRev);
		boolean rFwd = OI.get().getJoyDrive().getRawButton(btnRFwd);
		boolean rRev = OI.get().getJoyDrive().getRawButton(btnRRev);
		boolean zero = OI.get().getJoyDrive().getRawButton(btnZero);
		
		if (lFwd)
		{
			System.out.print("Forward     ");
			Catapult.getInstance().getVicL().set(0.3);
		}
		else if (lRev)
		{
			System.out.print("Reverse   ");
			Catapult.getInstance().getVicL().set(-0.3);
		}
		else
		{
			System.out.print("Off       ");
			Catapult.getInstance().getVicL().set(0.0);
		}
		
		if (rFwd)
		{
			System.out.println("Forward   ");
			Catapult.getInstance().getVicR().set(-0.3);
		}
		else if (rRev)
		{
			System.out.println("Reverse   ");
			Catapult.getInstance().getVicR().set(0.3);
		}
		else
		{
			System.out.println("Off       ");
			Catapult.getInstance().getVicR().set(0.0);
		}
		
		if (zero)
		{
			Catapult.getInstance().getEncL().setZero();
			Catapult.getInstance().getEncR().setZero();
		}
		
		System.out.println("encL = " + (4096 - Catapult.getInstance().getEncL().getEncoderPosition()));
		System.out.println("encR = " + Catapult.getInstance().getEncR().getEncoderPosition());
		System.out.println();
	}
}