package com.spcrobotics.catapult;

import com.spcrobotics.Constant;
import com.spcrobotics.peripheral.AbsoluteEncoder;
import edu.wpi.first.wpilibj.*;

public class Catapult
{
	// Singleton
	private static Catapult _singleton = null;
	
	// Member objects
	private Victor _vicL = null;
	private Victor _vicR = null;
	private AbsoluteEncoder _encL = null;
	private AbsoluteEncoder _encR = null;
	private CatapultPID _pidL = null;
	private CatapultPID _pidR = null;
	
	// Member vars
	private int _stateL = State.IDLE;
	private int _stateR = State.IDLE;
	
	private boolean enabled = true;
	
	private Catapult()
	{
		_vicL = new Victor(Constant.SLOT_LEFT, Constant.CAT_VIC_L);
		_vicR = new Victor(Constant.SLOT_RIGHT, Constant.CAT_VIC_R);
		
		DigitalOutput clk = new DigitalOutput(Constant.ABSENC_SLOT, Constant.ABSENC_PIN_CLK);
		DigitalOutput mosi = new DigitalOutput(Constant.ABSENC_SLOT, Constant.ABSENC_PIN_MOSI);
		DigitalInput miso = new DigitalInput(Constant.ABSENC_SLOT, Constant.ABSENC_PIN_MISO);
		
		DigitalOutput cs_r = new DigitalOutput(Constant.ABSENC_SLOT, Constant.ABSENC_PIN_R_CS);
		_encR = new AbsoluteEncoder(clk, mosi, miso, cs_r);
		
		DigitalOutput cs_l = new DigitalOutput(Constant.ABSENC_SLOT, Constant.ABSENC_PIN_L_CS);
		_encL = new AbsoluteEncoder(clk, mosi, miso, cs_l);
		
		_pidL = new CatapultPID("camL", 0.0012, 0.0, 0.0, _vicL, _encL, 1, true);	// 1, true
		_pidR = new CatapultPID("camR", 0.0015, 0.0, 0.0, _vicR, _encR, -1, false);	// -1, false
	}
	
	public static Catapult getInstance()
	{
		if (_singleton == null) {_singleton = new Catapult();}
		return _singleton;
	}
	
	public void run(boolean btnReset, boolean btnFire)
	{
		_stateL = _pidL.runStateMachine(_stateL, btnReset, btnFire);
		_stateR = _pidR.runStateMachine(_stateR, btnReset, btnFire);
	}
	
	public void ensureEnabled()
	{
		if (!enabled)
		{
			_pidL.enable();
			_pidL.getPIDController().enable();
			_pidL.forceEnable();
			_pidR.enable();
			_pidR.getPIDController().enable();
			_pidR.forceEnable();
			enabled = true;
			
			// Set state to idle (EXPERIMENTAL)
			_stateL = State.IDLE;
			_stateR = State.IDLE;
			
			System.out.println("Catapult enabled.");
		}
	}
	
	public void ensureDisabled()
	{
		if (enabled)
		{
			_pidL.disable();
			_pidL.getPIDController().disable();
			_pidL.forceDisable();
			_pidR.disable();
			_pidR.getPIDController().disable();
			_pidR.forceDisable();
			enabled = false;
			
			System.out.println("Catapult disabled.");
		}
	}
	
	public Victor getVicL() {return _vicL;}
	public Victor getVicR() {return _vicR;}
	public AbsoluteEncoder getEncL() {return _encL;}
	public AbsoluteEncoder getEncR() {return _encR;}
	
	public static class State
	{
		public static final int IDLE			= 1;
		public static final int RESETTING		= 2;
		public static final int READY			= 3;
		public static final int FIRING			= 4;
		
		private State() {}
	}

}
