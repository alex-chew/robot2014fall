package com.spcrobotics.catapult;

import com.spcrobotics.common.FlightRecorder;
import com.spcrobotics.peripheral.AbsoluteEncoder;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.command.PIDSubsystem;

public class CatapultPID
	extends PIDSubsystem
{
	private static final double ABS_TOL = 200;
	private static final double MAX_INPUT = 4096.0;
	private static final double MIN_INPUT = 0.0;
	private static final double MAX_OUTPUT = 1.0D;
	private static final double MIN_OUTPUT = 0.0D;
	
	// Constants
	private static final long IDLE_SP = 400;
	private static final long RESETTING_OFFSET = 500;
	private static final long READY_SP = 3650;
	
	String name = "";
	Victor _vic = null;
	AbsoluteEncoder _enc = null;
	int multiplier = 0;
	public long encPos = 0;
	boolean reverseEnc = false;
	
	boolean noOutput = false;
	
	public CatapultPID
	(
		String name, double kP, double kI, double kD,
		Victor v, AbsoluteEncoder e, int multiplier, boolean reverseEnc
	)
	{
		super(name, kP, kI, kD);
		super.setAbsoluteTolerance(ABS_TOL);
		
		this.getPIDController().setContinuous(true);
		this.getPIDController().setInputRange(MIN_INPUT, MAX_INPUT);
		this.getPIDController().setOutputRange(MIN_OUTPUT, MAX_OUTPUT);
		
		_vic = v;
		_enc = e;
		this.name = name;
		this.multiplier = multiplier;
		this.reverseEnc = reverseEnc;
		
		FlightRecorder.getInstance().writeHeader(name, String.valueOf(_vic.getChannel()), "null", "null", kP, kI, kD);
		this.enable();
		
		System.out.println(name + " P = " + this.getPIDController().getP());
	}
	
	public int runStateMachine(int state, boolean btnReset, boolean btnFire)
	{
		int nextState = state;
		
		switch (state)
		{
			case Catapult.State.IDLE:
				_vic.set(0.0);
				if (btnReset)
				{
					nextState = Catapult.State.RESETTING;
					print("IDLE >> RESETTING");
				}
				break;
			case Catapult.State.RESETTING:
				if (currSP() < READY_SP)
				{
					long nextSP = encPos + RESETTING_OFFSET;
					if (nextSP > READY_SP) {nextSP = READY_SP;}
					
					setSP(nextSP);
					nextState = Catapult.State.RESETTING;
				}
				else if (this.getPIDController().onTarget())
				{
					_vic.set(0.0);
					nextState = Catapult.State.READY;
					print("RESETTING >> READY");
				}
				// otherwise, let PIDController finish moving Victor
				break;
			case Catapult.State.READY:
				if (btnFire)
				{
					setSP(IDLE_SP);
					nextState = Catapult.State.FIRING;
					print("READY >> FIRING");
				}
				break;
			case Catapult.State.FIRING:
				if (this.getPIDController().onTarget())
				{
					_vic.set(0.0);
					nextState = Catapult.State.IDLE;
					print("FIRING >> IDLE");
				}
				break;
			default:
				print("Catapult state machine in unknown state! System will attempt exit.");
				System.exit(-1);
				break;
		}
		
		return nextState;
	}
	
	protected double returnPIDInput()
	{
		encPos = _enc.getEncoderPosition();
		if (reverseEnc)
		{
			encPos = 4096 - encPos;
			if (encPos == 4096) {encPos = 0;}
		}
		
		return encPos;
	}

	protected void usePIDOutput(double d)
	{
		if (noOutput)
		{
			System.out.println("PIDOutput blocked!");
		}
		else
		{
			System.out.println("PIDOutput is being used.");
			_vic.set(multiplier * trim(d));
			FlightRecorder.getInstance().writeEntry(name, encPos, super.getSetpoint(), d);
		}
	}
	
	public void forceDisable()
	{
		noOutput = true;
		System.out.println("Catapult force-disabled.");
	}
	
	public void forceEnable()
	{
		noOutput = false;
		System.out.println("Catapult force-enabled.");
	}
	
	protected void initDefaultCommand()
	{
		_vic.set(0.0);
	}
	
	private double currSP()
	{
		return this.getPIDController().getSetpoint();
	}
	
	private void setSP(double sp)
	{
		this.getPIDController().setSetpoint(sp);
	}
	
	private static void print(String msg)
	{
		System.out.println(msg);
		FlightRecorder.getInstance().write(msg);
	}
	
	double trim(double d)
	{
		if (d > MAX_OUTPUT) {return MAX_OUTPUT;}
		if (d < MIN_OUTPUT) {return MIN_OUTPUT;}
		return d;
	}
}
