package com.spcrobotics.pickup;

import com.spcrobotics.Constant;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.Solenoid;

public class ArmPistonController {
	
	private static ArmPistonController _singleton = null;
	private Compressor _compressor;
	private Solenoid _solenoidIn;
	private Solenoid _solenoidOut;
	
	private ArmPistonController() {
		_compressor = new Compressor(
				Constant.ARM_SLOT, Constant.ARM_COMP_PRESSURE_CHAN, // Pressure switch
				Constant.ARM_SLOT, Constant.ARM_COMP_RELAY_CHAN); // Compressor relay
		_solenoidIn = new Solenoid(Constant.ARM_SLOT, Constant.ARM_SOL_IN_CHAN);
		_solenoidOut = new Solenoid(Constant.ARM_SLOT, Constant.ARM_SOL_OUT_CHAN);
		
		startCompressor();
	}
	
	public static ArmPistonController getInstance() {
		if (_singleton == null) {_singleton = new ArmPistonController();}
		return _singleton;
	}
	
	public void startCompressor() {_compressor.start();}
	public void stopCompressor() {_compressor.stop();}
	
	public void armUp() {
		_solenoidIn.set(Constant.ARM_UP_IN);
		_solenoidOut.set(Constant.ARM_UP_OUT);
	}
	
	public void armDown() {
		_solenoidIn.set(Constant.ARM_DOWN_IN);
		_solenoidOut.set(Constant.ARM_DOWN_OUT);
	}
	
}
