package com.spcrobotics.pickup;

import com.spcrobotics.Constant;
import edu.wpi.first.wpilibj.Victor;

public class SpeedRoller
{
	private static SpeedRoller _singleton = null;
	private Victor _roller = null;
	
	private static final double inSpeed =   0.85;
	private static final double outSpeed =  -0.85;
	private static final double noSpeed =   0.0;
	
	private SpeedRoller()
	{
		_roller = new Victor(Constant.ROLLER_SLOT, Constant.ROLLER_CHANNEL);
	}
	
	public static SpeedRoller getInstance()
	{
		if (_singleton == null) {_singleton = new SpeedRoller();}
		return _singleton;
	}
	
	public void setIn()
	{
		_roller.set(inSpeed);
	}
	
	public void setOut()
	{
		_roller.set(outSpeed);
	}
	
	public void setOff()
	{
		_roller.set(noSpeed);
	}
	
	public double getSpeed()
	{
		return _roller.get();
	}
}
