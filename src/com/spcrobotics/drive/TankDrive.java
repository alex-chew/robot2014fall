package com.spcrobotics.drive;

import com.spcrobotics.Constant;
import com.spcrobotics.common.MathUtil;
import edu.wpi.first.wpilibj.RobotDrive;

public class TankDrive
{
	private static TankDrive _singleton = null;
	private RobotDrive _drive = null;
	
	private TankDrive()
	{
		_drive = new RobotDrive
		(
			Drivetrain.get().vicFL(),
			Drivetrain.get().vicRL(),
			Drivetrain.get().vicFR(),
			Drivetrain.get().vicRR()
		);
	}
	
	public static TankDrive getInstance()
	{
		if (_singleton == null) {_singleton = new TankDrive();}
		return _singleton;
	}
	
	public void execTele(double move, double rotate)
	{
		// Using normalize and scale function, transform input to make
		// controller less sensitive to small movements
		double adjM = MathUtil.normalizeScale(move,		Constant.DEADZONE_PS2MOV);
		double adjR = MathUtil.normalizeScale(rotate,	Constant.DEADZONE_PS2ROT);
		
		// Ensure motors don't receive inputs greater than 1 or less than -1
		adjM = trim(adjM);
		adjR = trim(adjR);
		
		// Invert move input to match controller
		_drive.arcadeDrive(-1 * adjM, adjR);
	}
	
	public double trim(double d)
	{
		if (d >= 1.0) {return 1.0;}
		if (d <= -1.0) {return -1.0;}
		return d;
	}
}
