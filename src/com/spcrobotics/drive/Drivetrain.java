package com.spcrobotics.drive;

import com.spcrobotics.Constant;
import edu.wpi.first.wpilibj.*;

public class Drivetrain
{
	private static Drivetrain _singleton = null;
	
	private Victor[] _victors = null;
	private Encoder[] _encoders = null;
	
	public static Drivetrain get()
	{
		if (_singleton == null) {_singleton = new Drivetrain();}
		return _singleton;
	}
	
	private Drivetrain()	 // For debugging
	{
		// @DEBUG
		System.out.println("Constructing Drivetrain");
		
		_victors = new Victor[4];
		_encoders = new Encoder[4];
		for (int i = 0; i < Constant.DRIVE_PWM.length; i++)
		{
			// @DEBUG
			System.out.println("Attempting to init vic/enc pair " + i);
			
			_victors[i] = new Victor(Constant.SLOT[i], Constant.DRIVE_PWM[i]);
			_encoders[i] = new Encoder
			(
				Constant.SLOT[i], Constant.DRIVE_ENC[i][0],
				Constant.SLOT[i], Constant.DRIVE_ENC[i][1]
			);
		}
		
		System.out.println("Attempting to init gyro @" + Constant.DRIVE_GYRO_SLOT + ":" + Constant.DRIVE_GYRO_CHAN);
//		_gyro = new Gyro(Constant.DRIVE_GYRO_SLOT, Constant.DRIVE_GYRO_CHAN);
//		FlightRecorder.getInstance().write("Initialzed PID subsystem \"" + "gyro" + "\", Victor channel = " + "1" + ", " + "gyro" + "QuadA=" + "1" + ", " + "gyro" + "QuadB=" + "1");
//		FlightRecorder.getInstance().write("gyro" + " parameters are P = " + Constant.DRIVE_GYRO_K_P_MULTIPLIER + ", I = " + "0" + ", D = " + "0");
	}
	
	public Victor vic(int i) {return _victors[i];}
	public Victor[] vicArray() {return _victors;}
	public Victor vicFL() {return vic(Constant.__FL__);}
	public Victor vicFR() {return vic(Constant.__FR__);}
	public Victor vicRL() {return vic(Constant.__RL__);}
	public Victor vicRR() {return vic(Constant.__RR__);}
	
	public Encoder enc(int i) {return _encoders[i];}
	public Encoder[] encArray() {return _encoders;}
	public Encoder encFL() {return enc(Constant.__FL__);}
	public Encoder encFR() {return enc(Constant.__FR__);}
	public Encoder encRL() {return enc(Constant.__RL__);}
	public Encoder encRR() {return enc(Constant.__RR__);}
	
	// NOTE: For the gyro, turning left is -ve, and turning right is +ve
//	public Gyro gyro() {return _gyro;}
	
//	public void resetGyro() {_gyro.reset();}
}
