package com.spcrobotics.drive;

import com.spcrobotics.Constant;
import com.spcrobotics.common.MathUtil;
import com.spcrobotics.common.Util;

public class MecanumDrive
{
	private static final boolean __DEBUG__ = false;
	
	public MecanumDrive() {}
	
	public void execTele(double rawMag, double rawDir, double rawRot)
	{
		// All inputs are adjusted here
		double mag = Util.curveInput(rawMag, Constant.DEADZONE_JOYMAG);
		double dir = MathUtil.directionToCartesian(rawDir);
		double rot = Util.curveInput(rawRot, Constant.DEADZONE_JOYROT);
		
		// Calculate wheel speeds
		double[] wheels = Util.calculateWheelsFromDirection(mag, dir, rot);
		for (int i = 0; i < Drivetrain.get().vicArray().length; i++)
		{
			// Mulitply to reverse wheel if necessary
			double sentSpeed = wheels[i] * Constant.DRIVE_VIC_MULTIPLIERS[i];
			
			// Set wheel
			Drivetrain.get().vic(i).set(sentSpeed);
			
//			System.out.println("SPEED " + i + " = " + sentSpeed);
		}
		
//		if (__DEBUG__) {System.out.println();}
	}
	
	// Set all wheels to respectively mirrored speeds
	public void execTest(double speed, boolean multiply)
	{
		for (int i = 0; i < Drivetrain.get().vicArray().length; i++)
		{
			Drivetrain.get().vic(i).set(speed *
				(multiply ? Constant.DRIVE_VIC_MULTIPLIERS[i] : 1));
		}
	}
}
