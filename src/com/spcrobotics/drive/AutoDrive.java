package com.spcrobotics.drive;

import com.spcrobotics.Constant;

public class AutoDrive
{
	private BaseSystem[] _pidSystem = new BaseSystem[4];
	private static AutoDrive _singleton = null;
	private static boolean initialized = false;
	
//	public static final double[][] DRIVE_PID =
//	{
//		{0.02,		0.0,		0.001},	// FL
//		{0.02,		0.0,		0.001},	// FR
//		{0.02,		0.0,		0.001},	// RL
//		{0.02,		0.0,		0.001}	// RR
//	};
	
//	public static final double[] DRIVE_ENC_DISTPERPULSE =
//	{
//		6 * Math.PI / 360,	// FL
//		6 * Math.PI / 250,	// FR
//		6 * Math.PI / 250,	// RL
//		6 * Math.PI / 250,	// RR
//	};

	private AutoDrive()
	{
		for (int i = 0; i < _pidSystem.length; i++)
		{
			// @DEBUG
			System.out.println("Attempting to init BaseSystem " + i);
			
			// BE CAREFUL! This is a hacky way to get the side of the Vic/enc
			// pair, as we assume the 1st and 3rd (indices 0 and 2) pairs are on
			// the left, and vice versa.
			boolean left = (i % 2 == 0);
			
			_pidSystem[i] = new BaseSystem(
				Constant.DRIVE_NAME[i], left,
				Constant.DRIVE_PID[i],
				Drivetrain.get().vic(i), ((Constant.DRIVE_VIC_MULTIPLIERS[i] > 0) ? false : true),
				Drivetrain.get().enc(i), Constant.DRIVE_ENC_DISTPERPULSE[i]);
			
			_pidSystem[i].setSetpoint(0.0);
			_pidSystem[i].enable();
		}
		
		initialized = true;
	}

	public static AutoDrive getInstance()
	{
		if (_singleton == null) {_singleton = new AutoDrive();}
		return _singleton;
	}
	
	public static boolean isInitialized()
	{
		return initialized;
	}

	public boolean driveStraight(double distance)
	{
		int systemsOnTarget = 0;
		int systemsRequiredForShutoff = 1;
		
		for (int i = 0; i < _pidSystem.length; i++)
		{
			_pidSystem[i].setSetpoint(distance);
			
			if (_pidSystem[i].getController().onTarget())
			{
				// Allows individual controllers to turn selves off
//				_pidSystem[i].getController().disable();
//				_pidSystem[i].resetIO();
				
				systemsOnTarget++;
			}
		}
		
		// Turns all off
		if (systemsOnTarget >= systemsRequiredForShutoff)
		{
			fullReset();
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void stop()
	{
		for (int i = 0; i < _pidSystem.length; i++)
		{
			_pidSystem[i].getController().disable();
			_pidSystem[i].setVictor(0.0);
		}
	}

	public void setRawSpeed(double speed)
	{
		for (int i = 0; i < _pidSystem.length; i++)
		{
			_pidSystem[i].setVictor(speed);
		}
	}
	
//	public double getGyroOffset(boolean left)
//	{
//		double roundedAngle = com.spcrobotics.common.MathUtil.round(
//				Drivetrain.get().gyro().getAngle()
//		);
//		
//		// Proportional const * abs(angle) * -1 (since we're *slowing* the wheel that's ahead)
//		double offset = -1 * Constant.DRIVE_GYRO_K_P_OFFSET * Math.abs(roundedAngle);
//		
//		if
//		(
//			(left && roundedAngle > 0) ||	// This is a left motor and robot is turned right
//			(!left && roundedAngle < 0)		// This is a right motor and the robot is turned left
//		)
//		{
//			return offset;
//		}
//		else {return 0.0D;} // Do nothing --> 0 (additive offset)
//	}
//	
//	public double getGyroMultiplier(boolean left)
//	{
//		double roundedAngle = com.spcrobotics.common.MathUtil.round(
//				Drivetrain.get().gyro().getAngle()
//		);
//		
//		if (Math.abs(roundedAngle) >= (1 / Constant.DRIVE_GYRO_K_P_MULTIPLIER))
//		{
//			// We dun screwed
//			return 1.0D;
//		}
//		
//		// 1 - (Proportional const * abs(angle)) --> If we're off by 20deg and
//		// const is 0.01, then mult would be 1 - 0.2, or 0.8
//		double multiplier = 1.0 - (double) (Constant.DRIVE_GYRO_K_P_MULTIPLIER * Math.abs(roundedAngle));
//		
//		if
//		(
//			(left && roundedAngle > 0) ||	// This is a left motor and robot is turned right
//			(!left && roundedAngle < 0)		// This is a right motor and the robot is turned left
//		)
//		{
//			return multiplier;
//		}
//		else {return 1.0D;} // Do nothing --> 1 = 100% (multiplier)
//	}
	
	public void fullReset()
	{
		/*
		 * THESE MUST BE IN THIS ORDER; the controllers must be reset before the
		 * encoders are reset, or else the controllers will try to move before
		 * turning off.
		 */
		disableControllers();
		resetIO();
	}
	
	public void disableControllers()
	{
		for (int i = 0; i < _pidSystem.length; i++)
		{
			_pidSystem[i].getController().setSetpoint(0.0);
			_pidSystem[i].getController().disable();
		}
	}
	
	public void resetIO()
	{
		for (int i = 0; i < _pidSystem.length; i++)
		{
			_pidSystem[i].resetIO();
		}
	}
	
	public BaseSystem getBaseSystem(int i)
	{
		return _pidSystem[i];
	}

}
