package com.spcrobotics.drive;

import com.spcrobotics.common.*;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.command.PIDSubsystem;
import edu.wpi.first.wpilibj.Victor;

public class BaseSystem
	extends PIDSubsystem
{
	private String PIDName = null;
	Encoder _encoder;
	Victor _victor;
	FlightRecorder _fr = null;
	
	private static final double percentOfTolerance = 10;
	private static final boolean useGyro = false;
	
	private double distance = 0.0; // @TODO
	private double maxOutput = 0.5;
	private double minOutput = -0.5;
	private double voltage = 0.0;
	private boolean reverse = false;
	
	private static long count = 0;
	
	public BaseSystem
	(
		String name, boolean left,
		double[] pid,
		Victor v, boolean victorReverse,
		Encoder e, double distPerPulse)
	{
		super(name, pid[0], pid[1], pid[2]);
		super.setPercentTolerance(percentOfTolerance);
		PIDName = name;
		
		_encoder = e;
		_encoder.setDistancePerPulse(distPerPulse);
		
		_victor = v;
		reverse = victorReverse;
		
		_fr = FlightRecorder.getInstance();
		_fr.writeHeader(PIDName, String.valueOf(v.getChannel()), "null", "null", pid[0], pid[1], pid[2]);
		
		this.resetIO();
	}
	
	
	public PIDController getController()
	{
		return this.getPIDController();
	}

	protected double returnPIDInput()
	{
		distance = _encoder.getDistance(); // in terms of distance
		if (reverse) {distance *= -1;}
		
//		distance = _encoder.getRate();
//		System.out.println(PIDName + " distance = "+ distance);
		
		return distance;
	}

	protected void usePIDOutput(double d)
	{
		if (!AutoDrive.isInitialized()) {return;}
		
		if (d < minOutput)		{d = minOutput;} // @DEBUG
		else if (d > maxOutput)	{d = maxOutput;}
		
//		if (useGyro)
//		{
//			voltage = d * (reverse ? -1 : 1) * AutoDrive.getInstance().getGyroMultiplier(left);
//		}
//		else
//		{
			voltage = d * (reverse ? -1 : 1);
//		}
		
		_victor.set(voltage);
		
//		System.out.println(PIDName + " voltage = "+ voltage);
		
		///////////////////// FR: CURR , DESIRED , CTRL ///////////////////////
//		String baseSystemMsg =
//			PIDName + ":" +
//			distance + "," +
//			super.getSetpoint() + ",";
//		
//		String gyroMsg =
//			"gyro" + ":" +
//			AutoDrive.getInstance().getGyroMultiplier(left) + "," +
//			AutoDrive.getInstance().getGyroMultiplier(left) + "," +
//			Drivetrain.get().gyro().getAngle();
//		
//		if (useGyro)
//		{
//			baseSystemMsg += (d * AutoDrive.getInstance().getGyroMultiplier(left));
//		}
//		else
//		{
//			baseSystemMsg += d;
//		}
//		
//		_fr.write(baseSystemMsg);
//		
//		if (useGyro)
//		{
//			_fr.write(gyroMsg);
//		}
		
		// Remove later
		_fr.write
		(
			PIDName + ":" +
			distance + "," +
			super.getSetpoint() + "," + d
		);
		
		_fr.write(PIDName + ":" + isOnTarget());
		
		count++;
		if (count % 4 == 0) {_fr.write("");}
	}

	protected void initDefaultCommand()
	{
		_victor.set(0.0);
	}
	
	public void victorReset() {_victor.set(0.0);}
	
	public void encoderReset()
	{
		_encoder.stop();
		_encoder.reset();
	}
	
	public final void resetIO()
	{
		victorReset();
		encoderReset();
	}
	
	/**
	 * Ensures PIDController is enabled, and encoder is started.
	 */
	public void ensureSystemEnabled()
	{
		/*
		 * THESE MUST BE IN THIS ORDER; the encoders must be set to 0 before
		 * enabling the controller or else it will try to move.
		 */
		_encoder.reset();
		_encoder.start();
		this.getController().setSetpoint(0.0);
		this.getController().enable();
	}
	
	public boolean isOnTarget()
	{
		return super.onTarget();
	}

	public void setVictor(double speed)
	{
		_victor.set(speed);
	}
}
